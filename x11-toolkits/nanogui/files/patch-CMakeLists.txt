--- CMakeLists.txt.orig	2022-04-10 13:30:19 UTC
+++ CMakeLists.txt
@@ -1,6 +1,8 @@
 cmake_minimum_required (VERSION 3.13..3.18)
 set(CMAKE_OSX_DEPLOYMENT_TARGET 10.13)
 
+include(CMakePackageConfigHelpers)
+
 # Extract project version from source
 file(STRINGS "${CMAKE_CURRENT_SOURCE_DIR}/include/nanogui/common.h"
   nanogui_version_defines REGEX "#define NANOGUI_VERSION_(MAJOR|MINOR|PATCH) ")
@@ -497,6 +504,12 @@ if (EXISTS /opt/vc/include)
   target_include_directories(nanogui PUBLIC /opt/vc/include)
 endif()
 
+# Find glfw if nanogui is not compiling it internally.
+if (NOT NANOGUI_BUILD_GLFW)
+  find_package(glfw3 CONFIG REQUIRED)
+  target_link_libraries(nanogui PUBLIC glfw)
+endif()
+
 if (NANOGUI_INSTALL)
   install(TARGETS nanogui
           LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
@@ -522,6 +535,7 @@ if (NANOGUI_INSTALL)
 
   set(NANOGUI_CMAKECONFIG_INSTALL_DIR "${CMAKE_INSTALL_DATAROOTDIR}/cmake/nanogui")
 
+  include(CMakePackageConfigHelpers)
   configure_package_config_file(
     resources/nanoguiConfig.cmake.in nanoguiConfig.cmake
     INSTALL_DESTINATION ${NANOGUI_CMAKECONFIG_INSTALL_DIR})
@@ -580,7 +594,7 @@ if (NANOGUI_BUILD_PYTHON)
         list(APPEND CMAKE_PREFIX_PATH "${_tmp_dir}")
       find_package(pybind11 CONFIG REQUIRED)
     else()
-      find_package(Python COMPONENTS Interpreter Development REQUIRED)
+      find_package(Python3 ${FREEBSD_PYTHON_DISTVERSION} EXACT REQUIRED COMPONENTS Interpreter Development REQUIRED)
 
       # Allow overriding the pybind11 library used to compile NanoGUI
       set(NANOGUI_PYBIND11_DIR "${CMAKE_CURRENT_SOURCE_DIR}/ext/pybind11"
